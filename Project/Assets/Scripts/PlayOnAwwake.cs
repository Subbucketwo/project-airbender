﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnAwwake : MonoBehaviour
{
    public GameObject videoPlayer;
    public int timeToStop;
    void OnAwake()
    {
        {
            videoPlayer.SetActive(true);
            Destroy(videoPlayer, timeToStop);
        }
    }
}
