﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoPlayerScene2 : MonoBehaviour
{
    public GameObject videoPlayer;
    public int timeToStop;

    void Start()
    {
        videoPlayer.SetActive(true);
        Destroy(videoPlayer, timeToStop);

    }
    void Update()
    {
        if (videoPlayer == null)
        {
            SceneManager.LoadScene(1);
        }
    }
}
