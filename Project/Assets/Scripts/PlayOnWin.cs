﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnWin : MonoBehaviour
{
    public GameObject videoPlayer;
    public int timeToStop;

    void Start()
    {
        videoPlayer.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Apple"))
        {
            videoPlayer.SetActive(true);
        }
        if (other.CompareTag("Apple") && other.CompareTag("Apple"))
        {
            videoPlayer.SetActive(true);
            Debug.Log("I have won");
        }
    }
}
