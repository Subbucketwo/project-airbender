﻿using UnityEngine;

public class Stairs : MonoBehaviour
{
    static int counter = 0;
    private int maxStairs = 10;
    // Start is called before the first frame update
    void Start()
    {

        counter++;
        if (counter < maxStairs)
            DoTheStairs();
    }

    private void DoTheStairs()
    {
        var myPos = transform.position;
        var go = Instantiate(gameObject, new Vector3(myPos.x, myPos.y + 1, myPos.z + 1), Quaternion.identity) as GameObject;
        go.name = "Stair no. " + counter;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
